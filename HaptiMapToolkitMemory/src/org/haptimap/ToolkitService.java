package org.haptimap;

import java.util.HashMap;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/* 
 * Wrapper service to create and initialise a single toolkit instance for the
 * running application and pass it to activities as and when they need it.
 * 
 * As currently implemented, the toolkit instance is initialised when the first
 * activity binds to the service, and deinitialised and destroyed when the last
 * activity unbinds. In the future it will probably be useful to extend this
 * so it runs as a background (unbound) service to support certain toolkit
 * functionality that runs in the background, i.e. does not require the screen
 * to be on and a user interacting visually with the device.
 */
public class ToolkitService extends Service
{
 
	private static final String TAG = ToolkitService.class.getSimpleName();
	
	/* When the service is first started, create and initialise an
     * instance of the toolkit which will then be passed to any 
     * activity that binds to the service. */
    @Override
    public void onCreate()
    {
        hm = new HaptiMap(this);

        if(hm.Initialise() != HaptiMap.SUCCESS)
            hm = null;
    }

    /* Called whenever an activity binds to the service. Returns an instance
     * of the toolkit binder which allows the activity to retrieve a
     * reference to the toolkit wrapper class and call toolkit functions. */
    @Override
    public IBinder onBind(Intent intent)
    {
        return tk_binder;  
    }

    /* Deinitialise the toolkit when the service is being shut down (i.e. the
     * final activity has unbound and there is no need to keep it running). */
    @Override
    public void onDestroy()
    {
        if(hm != null)
        {
            /* Uninitialise the toolkit and remove the reference to the
             * wrapper class so it can be garbage collected. */
            hm.Uninitialise();
            hm = null;
        }
    }

    /* This class exists only to allow bound activities to retrieve a 
     * reference to the toolkit instance hosted by this service. */
    public class ToolkitBinder extends Binder
    {
        public HaptiMap getHaptiMap()
        {
            return hm;
        }
    }

    /** Toolkit instance associated with this class */
    private HaptiMap hm;
    /** Single binder instance that will be passed to all activities binding
     *  to this service, to allow them to retrieve a reference to the active
     *  toolkit instance */
    private final IBinder tk_binder = new ToolkitBinder();
    
    private static HashMap<String, Long> kvpairs = new HashMap<String, Long>();
    
    /**
     * Static method to allow the jni to save pointers in the application context.
     * The JNI version does not work as expected
     * @param ctx The applications context
     * @param key for the store
     * @param value to store
     */
    @SuppressWarnings("unused")
    private static void JNISaveLong(Context ctx, String key, long value) {
    	Log.d(TAG, "JNISaveLong: "+key + ":" + value);
    	kvpairs.put(key, new Long(value));
    	Log.d(TAG, "Reached");
    }
    /**
     * Static method to receive the previously saved value
     * @param ctx The applications context
     * @param key for the store
     * @return
     */
    @SuppressWarnings("unused")
    private static long JNIGetLong(Context ctx, String key) {
    	long retVal = kvpairs.get(key).longValue();
    	Log.d(TAG, "JNIGetLong("+key+"): "+retVal);
    	return retVal;
    }
}
