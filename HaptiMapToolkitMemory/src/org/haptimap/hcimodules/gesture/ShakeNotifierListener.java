package org.haptimap.hcimodules.gesture;

/**
 * Listener used to receive events when registered with 
 * {@link ShakeNotifier#setOnShakeListener(ShakeNotifierListener)}
 *
 * @author Miguel Molina, October 2011
 */
public interface ShakeNotifierListener {

	/**
	 * Event fired when a shake is detected
	 */
	public void onShakeDetected(double force);
	public void onTurnDetected();
	public void allowTurn();
	public void disallowTurn();
	public void moveLeft();
	public void moveRight();
	public void moveUp();
	public void moveDown();
}
