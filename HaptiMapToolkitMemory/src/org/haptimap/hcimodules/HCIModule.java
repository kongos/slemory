package org.haptimap.hcimodules;

import android.content.Context;

public abstract class HCIModule {

	protected Context context;
	
	public HCIModule(Context context) {
		this.context = context;
	}
	
	public abstract void onStart();
	public abstract void onPause();
	public abstract void onResume();
	public abstract void onStop();
	public abstract void onDestroy();
	
}
