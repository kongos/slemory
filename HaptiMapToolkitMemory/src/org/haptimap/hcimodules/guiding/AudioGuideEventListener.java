package org.haptimap.hcimodules.guiding;

/**
 * Used for receiving notifications from the {@link AudioGuide} module on different events. 
 * These methods are called if the current listener has been registered with the Audio Guide
 * module using the {@link AudioGuide#registerAudioGuideEventListener(AudioGuideEventListener)}
 * method.
 * 
 * @author Miguel Molina, August 2011
 */
public interface AudioGuideEventListener extends GuideEventListener {

	/**
	 * Called when the destination is reached. 
	 */
	void onDestinationReached();


}
