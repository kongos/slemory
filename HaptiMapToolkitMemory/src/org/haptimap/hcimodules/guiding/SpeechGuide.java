package org.haptimap.hcimodules.guiding;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.haptimap.hcimodules.HCIModule;
import org.haptimap.hcimodules.util.WayPoint;

import android.Manifest.permission;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;

/**
 * This {@link HCIModule} produces speech according to the deviation from a target. It takes advantage on where 
 * the user is currently pointing at with the handheld device and maps the deviation with a direction.
 * The spoken direction is a recommendation on where about to go. The destination point is a {@link WayPoint}, which
 * might be added with the constructor or at a later occasion with one of the setNextDestination(...) methods.</p>
 * 
 * The available constructors in this class are:
 * <blockquote>
 * <pre>
 * SpeechGuide(Context context)
 * SpeechGuide(Context context, String destinationName, String latitude, String longitude)
 * SpeechGuide(Context context, WayPoint destination)
 * </pre>
 * </blockquote>
 * 
 * <p>While interacting with this module, the current deviation from the target will be processed and the 
 * speech mapped to the deviation block will be spoken. The speech uses the default {@link TextToSpeech} installed
 * in the device and there might be limitations in the available languages supported by the engine.</p>
 * 
 * <p>The speech is played in a continuously default rate and this rate should be enough for the interaction. 
 * The default rate can be override using {@link #setRateInterval(int)} and it can be restored to the default 
 * value using {@link #setDefaultRateInterval()}. If the rate is too small, there will be not enough time for the
 * {@link TextToSpeech} to finish the current speech and it will be interrupted.</p>
 * 
 * <p>This {@link HCIModule} does not use support distances for rate intervals.</p>
 * 
 * <p>The different zones for the speech feedback are mapped according to the following angles:</p>
 * 
 * <blockquote><pre>
 * AHEAD			: if deviation &lt |23| degrees
 * LEFT DEVIATION	: if 150 &lt deviation &lt 23 degrees
 * RIGHT DEVIATION	: if 210 &lt deviation &lt 337 degrees
 * BEHIND			: if 150 &lt deviation &lt 210 
 * </pre></blockquote>
 * 
 * <p>The class can also be used together with an {@link SpeechGuideEventListener}, which will
 * be called on different events. </p>
 * 
 * <p>When a destination is reached, besides a broadcast which can be received with a 
 * {@link BroadcastReceiver} and registered with the action {@link Guide#ACTION_DESTINATION_REACHED},
 * the SpeechGuide will also call the {@link SpeechGuideEventListener#onDestinationReached()} event and a 
 * destination speech will be played. Once the destination is reached, the module will go into the pause state.</p>
 * 
 * <p>The module follows a state machine, which can be compared to the regular Android approach. The
 * states that are included are: {@link #onStart()} -> {@link #onResume()} -> {@link #onPause()} -> 
 * {@link #onStop()} and {@link #onDestroy()}</p>
 * 
 * <p>For further details about the different states, please read the {@link HCIModule}.</p> 
 * 
 * <p>The simplest way in using this module is by <strong>creating</strong> this class with the 
 * {@link #SpeechGuide(Context, WayPoint)} constructor with the <strong>first destination</strong> to be 
 * guided to. The interaction does not begin until the device gets a GPS fix. Once the <strong>destination 
 * is reached</strong> the {@link SpeechGuideEventListener} calls the 
 * {@link SpeechGuideEventListener#onDestinationReached()} and a destination string tune will be played.<p> 
 * 
 * <p>Once you reach your destination and the module is in the pause state, you can continue the interaction by
 * adding the next destination. The module will automatically resume and no {@link #onResume()} is necessary.
 * The next destination might be added using one of the following methods:
 * 
 * <blockquote><pre>
 * {@link #setNextDestination(WayPoint)}
 * {@link #setNextDestination(String, double[])}
 * {@link #setNextDestination(String, android.location.Location)}
 * {@link #setNextDestination(String, String, String)}
 * </pre></blockquote>
 *  
 * <p>This module <strong>must</strong> have the following permissions in the AndroidManifest.xml file:</p>
 * <blockquote><pre> 
 * {@link permission#ACCESS_FINE_LOCATION}</li>
 * </pre></blockquote>	
 * 
 * <p>and the {@link TextToSpeech} must have been installed and the access to the SDCard must be possible.
 * 
 * @see HCIModule
 * @see SpeechGuideEventListener
 * 
 * @author Miguel Molina, August 2011
 */
public class SpeechGuide extends Guide{

	private static final String TAG = "SpeechGuide";
	private static final boolean DEBUG = false;	

	//The max deviation for the different zones
	private static final int MAX_AHEAD_DEVIATION = 23;
	private static final int MAX_RIGHT_DEVIATION = 150;
	private static final int MAX_LEFT_DEVIATION = 210;

	/**
	 * The default speech rate. The value might be too long/short depending on the language used.
	 */
	private static final long DEFAULT_SPEECH_RATE_INTERVAL = 5000;

	/**
	 * The size of the string array expected when adding a custom language which is not available
	 * among the default languages 
	 */
	public static final int DEFAULT_DIRECTION_STRING_ARRAY_SIZE = 5; 

	//Default languages. More languages might be added/removed/modified after preference
	private static final Locale GERMAN = Locale.GERMAN;
	private static final Locale ENGLISH = Locale.UK;
	private static final Locale SPANISH = new Locale("es", "ES");
	private static final Locale SWEDISH = new Locale("sv", "SE");

	//Map containing strings with directions
	private Map<Course, String> mDirections;

	//Map containing the available languages.
	private Map<Locale, Map<Course, String>> mAvailableLanguages;

	//TTS values
	private boolean ttsInitialized;
	private boolean languageIsAvailable;
	private Locale currentLanguage;
	private TextToSpeech tts;

	//Constants used when creating string values.
	private static final int VALUE_NOT_VALID = 0x10;
	private static final int SPEAK_NAME_DISTANCE_COURSE = 0x20;
	private static final int SPEAK_NAME_COURSE = 0x30;
	private static final int SPEAK_COURSE = 0x40;
	private int prevSpeakValue;

	private Course mCurrentCourse;
	private Course mPrevCourse;

	//The listener
	private SpeechGuideEventListener mSpeechGuideEventListener;

	/**
	 * Default constructor used to initiate this {@link HCIModule}. The context expected is the one 
	 * obtained by using {@link Activity#getApplicationContext()} If other {@link Context} is used, the 
	 * module might not operate if moving from the current {@link Activity} from where it has been created.
	 * 
	 * @param context The application's context 
	 */
	public SpeechGuide(Context context) {
		super(context);
		initLocalValues();
	}

	/**
	 * Default constructor used to initiate this {@link HCIModule}. The context expected is the one 
	 * obtained by using {@link Activity#getApplicationContext()} If other {@link Context} is used, the 
	 * module might not operate if moving from the current {@link Activity} from where it has been created.
	 * 
	 * @param context The application's context
	 * @param destination A {@link WayPoint} representing the destination
	 */
	public SpeechGuide(Context context, WayPoint wayPoint){
		super(context, wayPoint);
		initLocalValues();
		this.onStart();
	}

	/**
	 * Default constructor used to initiate this {@link HCIModule}. The context expected is the one 
	 * obtained by using {@link Activity#getApplicationContext()} If other {@link Context} is used, the 
	 * module might not operate if moving from the current {@link Activity} from where it has been created.
	 * 
	 * @param context The application's context
	 * @param destinationName The destination's name 
	 * @param latitude The destination's latitude in WGS84 decimal format
	 * @param longitude The destination's longitude in WGS84 decimal format
	 */
	public SpeechGuide(Context context, String destinationName, String latitude, String longitude){
		super(context, destinationName, latitude, longitude);
		initLocalValues();
		this.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
		stopTTS();
	}

	@Override
	public void onResume() {
		super.onResume();
		initTTS();
	}

	@Override
	public void onStop() {
		super.onStop();
		stopTTS();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopTTS();
	}

	/**
	 * Initializes local values
	 */
	private void initLocalValues() {
		this.currentLanguage = ENGLISH;
		this.mAvailableLanguages = new HashMap<Locale, Map<Course,String>>();
		initDefaultLanguages();
		this.languageIsAvailable = setDefaultLanguage();
		initTTS();	
	}

	/**
	 * Initializes the TTS and calls the {@link #onPrepared()}
	 */
	private void initTTS(){
		if(tts == null){
			tts = new TextToSpeech(context, new OnInitListener() {

				public void onInit(int status) {
					if(status == TextToSpeech.SUCCESS){
						tts.setLanguage(currentLanguage);
						tts.setPitch(0.8f);
						tts.setSpeechRate(1.0f);
						ttsInitialized = true;
					} else {
						ttsInitialized = false;
					}
				}
			});
			if(ttsInitialized) onPrepared();
		}
	}

	private void restartTTS(){
		stopTTS();
		initTTS();
	}

	private void stopTTS(){
		if(ttsInitialized && tts != null){
			tts.stop();
			tts.shutdown();
			ttsInitialized = false;
			tts = null;
			prevSpeakValue = VALUE_NOT_VALID;
		}
		return;
	}

	/**
	 * Sets the default tts engine.
	 */
	public void setDefaultTextToSpeechEngine(){
		this.tts = null;
		initTTS();
	}

	/**
	 * Sets a {@link TextToSpeech} engine of preference.
	 * 
	 * @param tts The {@link TextToSpeech} implementation that one would like to use.
	 */
	public void setCustomTextToSpeechEngine(TextToSpeech tts){
		if(tts == null){
			throw new IllegalArgumentException("tts = null");
		}
		stopTTS();
		this.tts = tts;
		this.ttsInitialized = true;
	}

	/**
	 * This method might fail in setting the current language depending on the available languages on the tts engines. 
	 * In those cases, the result might sound in another way than expected, since the words will be pronounced different.
	 * For example, the word "Paris" pronounced in French or in English wil sound different depending on the language set.
	 * 
	 * @return whether the language is available among the available languages in the current class.
	 */
	private boolean setDefaultLanguage(){
		this.mDirections = mAvailableLanguages.get(currentLanguage);
		return (this.mDirections != null);
	}

	/**
	 * Initializes a sample of languages available throught this class.
	 */
	private void initDefaultLanguages(){
		mAvailableLanguages.put(GERMAN, getGermanValues());
		mAvailableLanguages.put(ENGLISH, getEnglishValues());
		mAvailableLanguages.put(SPANISH, getSpanishValues());
		mAvailableLanguages.put(SWEDISH, getSwedishValues());
	}

	/**
	 * Interrupts the current TextToSpeech session, if busy, 
	 * and discards the queue.
	 */
	public void stopTalking(){
		if(tts.isSpeaking()){
			tts.stop();
		}
		return;
	}

	/**
	 * General method used to speak by flushing the current queue and adding the specified string.
	 * 
	 * @param speak The string that will be spoken
	 */
	public void talk(String speak){
		if(ttsInitialized){
			tts.speak(speak, TextToSpeech.QUEUE_FLUSH, null);
		}
		return;
	}

	/**
	 * Sets the preferred language among the default languages. The value can be found in the
	 * Language and represents the current values available as defualt.
	 * If other languages have been added, they will be found/set using the 
	 * {@link #setPreferredCustomLanguage(Locale)}, using the same {@link Locale} as they were
	 * added.
	 *  
	 * @param language The constant {@link Language} value for the language
	 * @return Whether the language is set.
	 * 
	 * @see #setPreferredCustomLanguage(Locale)
	 * @see Language
	 */
	public boolean setPreferredLanguage(Language language){

		switch(language){

		case ENGLISH:
			this.currentLanguage = ENGLISH;
			break;

		case GERMAN:
			this.currentLanguage = GERMAN;
			break;

		case SPANISH:
			this.currentLanguage = SPANISH;
			break;

		case SWEDISH:
			this.currentLanguage = SWEDISH;
			break;

		default:
			Log.e(TAG, "Unknown language type " + language);
			return false;
		}

		this.mDirections = this.mAvailableLanguages.get(this.currentLanguage);
		restartTTS();		
		this.languageIsAvailable = (this.mDirections != null);
		if(this.languageIsAvailable){
			restartTTS();
			onLanguageChanged();
		}
		return this.languageIsAvailable;
	}

	/**
	 * Sets the preferred language among the available languages. If the language does not exist,
	 * the last known language will remain as preferred.
	 * 
	 * @param locale The {@link Locale} representing the desired language
	 * @return Whether the language was successfully set.
	 */
	public boolean setPreferredCustomLanguage(Locale locale){
		boolean ans = mAvailableLanguages.containsKey(locale);
		if(ans){
			this.currentLanguage = locale;
			this.mDirections = mAvailableLanguages.get(locale);
			this.languageIsAvailable = ans;
			restartTTS();			
			onLanguageChanged();
		} else {
			//Do nothing and the last know language will remain.
			Log.e(TAG, "language " + locale.toString() + " not available");
		}
		return ans;
	}

	/**
	 * Adds a custom language to the available languages in this class. The Map with 
	 * directions should be filled in with all the {@link Course}'s values in order to 
	 * acquire these while interacting. If any course if missing, a "null" value will be spoken.
	 * 
	 * @param language {@link Locale} representing the language for these values
	 * @param values {@link Course} values
	 * 
	 * @throws IllegalArgumentException if language is null
	 * @throws IllegalArgumentException if values is null
	 * @throws IllegalArgumentException if values is empty
	 */
	public void addCustomLanguage(Locale language, Map<Course, String> values){
		if(language == null){
			throw new IllegalArgumentException("language is null");
		}
		if(values == null){
			throw new IllegalArgumentException("values is null");
		}
		if(values.isEmpty()){
			throw new IllegalArgumentException("values is empty");
		}
		Map<Course, String> tmp = new HashMap<SpeechGuide.Course, String>(values);
		this.mAvailableLanguages.put(language, tmp);
	}

	/**
	 * Adds a custom language to the available languages in this class. The string array with 
	 * directions should be filled in with all the {@link Course}'s values in order to acquire these while 
	 * interacting. The order of these values is as follow:
	 * 
	 * <ul>
	 * <li>directions[0] = STRAIGHT</li>
	 * <li>directions[1] = LEFT</li>
	 * <li>directions[2] = RIGHT</li>
	 * <li>directions[3] = TURN_AROUND</li>
	 * <li>directions[4] = ARRIVED</li>
	 * </ul>
	 * 
	 * @param locale {@link Locale} representing the language for these values
	 * @param directions Array with strings mapped to course values
	 * 
	 * @throws IllegalArgumentException if language is null
	 * @throws IllegalArgumentException if the array size is &lt {@link #DEFAULT_DIRECTION_STRING_ARRAY_SIZE}
	 */
	public void addCustomLanguage(Locale language, String[] directions){
		if(language == null){
			throw new IllegalArgumentException("language is null");
		}
		if(directions.length < DEFAULT_DIRECTION_STRING_ARRAY_SIZE){
			throw new IllegalArgumentException("size < " + DEFAULT_DIRECTION_STRING_ARRAY_SIZE);
		}
		Map<SpeechGuide.Course, String> tmp = new HashMap<SpeechGuide.Course, String>();
		tmp.put(Course.STRAIGHT, directions[0]);
		tmp.put(Course.LEFT, directions[1]);
		tmp.put(Course.RIGHT, directions[2]);
		tmp.put(Course.TURN_AROUND, directions[3]);
		tmp.put(Course.ARRIVED, directions[4]);

		this.mAvailableLanguages.put(language, tmp);
		if(DEBUG) Log.i(TAG, "language=" + language.toString());	
	}

	/**
	 * @return the current language used by the {@link TextToSpeech}
	 */
	public Locale getCurrentLanguage(){
		return this.currentLanguage;
	}

	/**
	 * Creates an array with the available {@link Locale} values in this class
	 * 
	 * @return The available languages supported
	 */
	public Locale[] getAvailableLanguages(){
		return mAvailableLanguages.keySet().toArray(new Locale[0]);
	}

	//English values
	private Map<SpeechGuide.Course, String> getEnglishValues(){
		Map<SpeechGuide.Course, String> tmp = new HashMap<SpeechGuide.Course, String>();
		tmp.put(Course.STRAIGHT, "keep straight");
		tmp.put(Course.LEFT, "keep left");
		tmp.put(Course.RIGHT, "keep right");
		tmp.put(Course.TURN_AROUND, "turn around");
		tmp.put(Course.ARRIVED, "arrived at");

		return tmp;
	}

	//Swedish values
	private Map<SpeechGuide.Course, String> getSwedishValues(){
		Map<SpeechGuide.Course, String> tmp = new HashMap<SpeechGuide.Course, String>();
		tmp.put(Course.STRAIGHT, "rakt fram");
		tmp.put(Course.LEFT, "h�ll till v�nster");
		tmp.put(Course.RIGHT, "h�ll till h�ger");
		tmp.put(Course.TURN_AROUND, "v�nd dig om");
		tmp.put(Course.ARRIVED, "kommit fram till");
		return tmp;
	}

	//German values
	private Map<SpeechGuide.Course, String> getGermanValues(){
		Map<SpeechGuide.Course, String> tmp = new HashMap<SpeechGuide.Course, String>();
		tmp.put(Course.STRAIGHT, "geradeaus");
		tmp.put(Course.LEFT, "halten Sie sich links");
		tmp.put(Course.RIGHT, "halten Sie sich rechts");
		tmp.put(Course.TURN_AROUND, "drehen Sie sich um");
		tmp.put(Course.ARRIVED, "Angekommen am");

		return tmp;
	}

	//Spanish values
	private Map<SpeechGuide.Course, String> getSpanishValues(){
		Map<SpeechGuide.Course, String> tmp = new HashMap<SpeechGuide.Course, String>();
		tmp.put(Course.STRAIGHT, "seguir recto");
		tmp.put(Course.LEFT, "mant�ngase a la izquierda");
		tmp.put(Course.RIGHT, "mant�ngase a la derecha");
		tmp.put(Course.TURN_AROUND, "girar media vuelta");
		tmp.put(Course.ARRIVED, "lleg� a");

		return tmp;
	}	

	/**
	 * enum representing the default languages available through this class. These values might be used
	 * when setting a preferred language.
	 *
	 *@see SpeechGuide#setPreferredLanguage(Language)
	 */
	public enum Language{
		ENGLISH,
		GERMAN,
		SWEDISH,
		SPANISH
	}

	/**
	 * enum representing the available values for directions. These values must be presented for a 
	 * full interaction experience when creating a custom language map.
	 * 
	 * @see SpeechGuide#addCustomLanguage(Locale, Map)
	 * @see SpeechGuide#addCustomLanguage(Locale, String[])
	 */
	public enum Course{
		STRAIGHT,
		LEFT,
		RIGHT,
		TURN_AROUND,
		ARRIVED
	}

	@Override
	protected void performAction() {
		talk(createSpeechString());
	}

	/**
	 * Creates the {@link Course} value depending on the deviation.
	 * 
	 * @return The current {@link Course} value
	 */
	private Course createDirection(){
		mPrevCourse = mCurrentCourse; //We save our old state
		if((mDeviation < MAX_AHEAD_DEVIATION)||(mDeviation > (360-MAX_AHEAD_DEVIATION))){
			return Course.STRAIGHT;
		} else if (mDeviation < MAX_RIGHT_DEVIATION){
			return Course.RIGHT;
		} else if (mDeviation > MAX_LEFT_DEVIATION){
			return Course.LEFT;			
		}else{
			return Course.TURN_AROUND;
		}
	}

	/**
	 * Creates the string that will be used for speech.
	 *  
	 * @return The string that will be spoken
	 */
	private String createSpeechString(){
		int what = VALUE_NOT_VALID;
		String st = "";

		this.mCurrentCourse = createDirection();

		if((mCurrentCourse == mPrevCourse)){
			if(prevSpeakValue == SPEAK_COURSE || prevSpeakValue == VALUE_NOT_VALID){
				prevSpeakValue = what = SPEAK_NAME_DISTANCE_COURSE;
			} else if (prevSpeakValue == SPEAK_NAME_DISTANCE_COURSE){
				prevSpeakValue = what = SPEAK_NAME_COURSE;
			} else if (prevSpeakValue == SPEAK_NAME_COURSE){
				prevSpeakValue = what = SPEAK_COURSE;
			} 
			else {
				Log.e(TAG, "value not valid");
				what = VALUE_NOT_VALID;
			}
		} else {
			prevSpeakValue = what = SPEAK_NAME_DISTANCE_COURSE;
		}

		if(languageIsAvailable){

			switch(what){

			case SPEAK_NAME_DISTANCE_COURSE:
				st = mDestination.getName() + " " + (int)mDistanceToDestination + ", " + mDirections.get(mCurrentCourse);
				break;

			case SPEAK_NAME_COURSE:
				st = mDestination.getName() + ", " +  mDirections.get(mCurrentCourse);
				break;

			case SPEAK_COURSE:
				st = mDirections.get(mCurrentCourse);
				break;

			default:
				Log.e(TAG, "value not valid:" + what);
			}
		}
		return st;
	}

	/**
	 * @return The string value when a destination is reached. 
	 */
	private String createOnDestinationReached(){
		return mDirections.get(Course.ARRIVED) + " " + this.mDestination.getName();
	}

	/**
	 * @return <li> The recommended rate interval before the next call to this module
	 */
	@Override
	public long getRateInterval(){		
		long ans;
		if(!mHasCustomPostDelayValue){
			ans = DEFAULT_SPEECH_RATE_INTERVAL;
		} else {
			ans = mCustomPostDelayValue;
		}
		return ans;		
	}

	@Override
	public void setDefaultRateInterval() {
		this.mHasCustomPostDelayValue = false;
		this.mCustomPostDelayValue = (int) DEFAULT_SPEECH_RATE_INTERVAL;
		onRateIntervalChanged(DEFAULT_SPEECH_RATE_INTERVAL);
	}

	@Override
	public void setNextDestination(String destinationName, double[] latlon) {
		super.setNextDestination(destinationName, latlon);
		this.onResume();
	}

	@Override
	public void setNextDestination(String destinationName, Location destination) {
		super.setNextDestination(destinationName, destination);
		this.onResume();
	}

	@Override
	public void setNextDestination(String destinationName, String latitude,
			String longitude) {
		super.setNextDestination(destinationName, latitude, longitude);
		this.onResume();
	}

	@Override
	public void setNextDestination(WayPoint destination) {
		super.setNextDestination(destination);
		this.onResume();
	}

	/**
	 * Registers a {@link SpeechGuideEventListener} listener which is called on various events.
	 * 
	 * @param mSpeechGuideEventListener The listener that will be registered for callbacks
	 * 
	 * @throws IllegalArgumentException if the listener is null
	 * 
	 * @see {@link SpeechGuideEventListener}
	 */
	public void registerSpeechGuideEventListener(SpeechGuideEventListener mSpeechGuideEventListener){
		if(mSpeechGuideEventListener == null){
			throw new IllegalArgumentException("mSpeechGuideEventListener==null"); 
		}
		this.mSpeechGuideEventListener = mSpeechGuideEventListener;
	}

	/**
	 * Unregister the Listener, if it has been registered before, and no more callbacks will
	 * be sent.
	 * 
	 * @see {@link #registerSpeechGuideEventListener(SpeechGuideEventListener)}
	 */
	public void unregisterSpeechGuideEventListener(){
		this.mSpeechGuideEventListener = null;
	}

	/**
	 * Method used to send an event when the module is prepared. It does also happen if the language is changed. 
	 * 
	 * Used to call the {@link SpeechGuideEventListener#onPrepared(boolean)} if the listener has been registered.
	 * 
	 * @see #registerSpeechGuideEventListener(SpeechGuideEventListener)
	 */
	private void onPrepared() {
		if(mSpeechGuideEventListener != null){
			mSpeechGuideEventListener.onPrepared(ttsInitialized);
		}
	}

	/** 
	 * Used to call an event if the {@link SpeechGuideEventListener} has been registered.
	 * 
	 * @see #registerSpeechGuideEventListener(SpeechGuideEventListener)
	 */
	@Override
	protected void onRateIntervalChanged(long millis) {
		if(mSpeechGuideEventListener != null){
			mSpeechGuideEventListener.onRateIntervalChanged(millis);
		}
	}

	/**
	 * Event called when a language has been changed if the {@link SpeechGuideEventListener} has been registered. 
	 * 
	 * @see #registerSpeechGuideEventListener(SpeechGuideEventListener)
	 */
	private void onLanguageChanged() {
		if(this.languageIsAvailable){
			if(mSpeechGuideEventListener != null){
				mSpeechGuideEventListener.onLanguageChanged(this.currentLanguage);
			}
		}		
	}

	/**
	 * Event called when the destination has been reached. Besides the event, a broadcast is also sent
	 * and the module will enter the pause state.
	 * 
	 * @see SpeechGuideEventListener#onDestinationReached()
	 */
	@Override
	protected void onDestinationReached() {
		talk(createOnDestinationReached());
		if(mSpeechGuideEventListener != null){
			this.mSpeechGuideEventListener.onDestinationReached();
		}
		this.prevSpeakValue = VALUE_NOT_VALID;
		context.sendBroadcast(new Intent(ACTION_DESTINATION_REACHED));
		mHandler.postDelayed(new Runnable() {
			
			public void run() {
				SpeechGuide.this.onPause();
				
			}
		}, DEFAULT_SPEECH_RATE_INTERVAL);
	}

}
