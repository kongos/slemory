package org.haptimap.hcimodules.guiding;

import java.util.Locale;

/**
 * Used for receiving notifications from the {@link SpeechGuide} module on different events. 
 * These methods are called if the current listener has been registered with the {@link SpeechGuide}
 * module using the {@link SpeechGuide#registerSpeechGuideEventListener(SpeechGuideEventListener)}
 * method.
 * 
 * @author Miguel Molina, August 2011
 */
public interface SpeechGuideEventListener extends GuideEventListener{

	/**
	 * Called when a language has been changed. 
	 * 
	 * @param locale The {@link Locale} representing the current language used by the module
	 */
	void onLanguageChanged(Locale locale);

	/**
	 * Called when the destination is reached. 
	 */
	void onDestinationReached();

}
