package org.haptimap.hcimodules.guiding;

import org.haptimap.HaptiMap;
import org.haptimap.ToolkitService;
import org.haptimap.ToolkitService.ToolkitBinder;
import org.haptimap.hcimodules.HCIModule;

import android.os.Handler;
import android.os.Vibrator;


/**
 * Used for receiving notifications from the {@link HapticGuide} {@link HCIModule} module when
 * different events are fired. These methods are called if the
 * HapticGuideEventListener has been registered with the HapticGuide using
 * {@link HapticGuide#registerHapticGuideEventListener(HapticGuideEventListener)}
 * method.
 * 
 * @author Miguel Molina, July 2011
 */
public interface HapticGuideEventListener {

	/**
	 * Called when the rate interval changes. It might be useful together with a {@link Runnable}
	 * and and {@link Handler}.
	 *  
	 * @param millis The milliseconds until the next interaction.
	 */
	void onRateIntervalChanged(int millis);

	/**
	 * Called when the HapticGuide is connected or disconnected to the {@link ToolkitService} and the 
	 * {@link HaptiMap} is initialized or uninitialized.
	 *  
	 * @param onPrepared The status of the {@link ToolkitService} {@link ToolkitBinder}.
	 * 
	 * @see {@link HaptiMap}
	 * @see {@link ToolkitService}
	 * @see {@link ToolkitBinder}
	 */
	void onPrepared(boolean onPrepared);

	/**
	 * Called when the destination is reached. A small pattern used for a {@link Vibrator} 
	 * is fired.
	 * 
	 * @param pattern A long[] containing the pattern that can be used in the {@link Vibrator}.
	 * 
	 * @see {@link HapticGuide#DESTINATION_REACHED}
	 */
	void onDestinationReached(long[] pattern);
	
}
