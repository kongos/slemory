package org.haptimap.hcimodules.guiding;

import org.haptimap.hcimodules.HCIModule;

/**
 * Used for receiving notifications from the Guide Modules on different events. 
 * This interface could be used as a default block for other Guide listeners.
 * 
 * @author Miguel Molina, August 2011
 */
public interface GuideEventListener {

	/**
	 * Called when this {@link HCIModule} is ready to be used
	 *  
	 * @param onPrepared The status of this {@link HCIModule}.
	 * 
	 */
	void onPrepared(boolean onPrepared);		

	/**
	 * Called when the rate interval changes giving the time in milliseconds.
	 * 
	 * @param millis The time in milliseconds 
	 */
	void onRateIntervalChanged(long millis);

}
