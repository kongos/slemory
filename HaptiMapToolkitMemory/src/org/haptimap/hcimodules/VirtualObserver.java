package org.haptimap.hcimodules;

import android.content.Context;

public class VirtualObserver extends HCIModule {

	public VirtualObserver(Context ctx, String host, short port, String url, String uid, String aid) {
		super(ctx);
		setConfigOption(ConfigOption.HOST, host);
		setConfigOption(ConfigOption.PORT, port);
		setConfigOption(ConfigOption.URL, url);
		setConfigOption(ConfigOption.AID, aid);
		setConfigOption(ConfigOption.UID, uid);
	}

	public native void onStart();
	public native void onPause();
	public native void onResume();
	public native void onStop();
	public native void onDestroy();

	public native int setConfigOption(ConfigOption op, Object o);
	public native int setDataField(DataField df, Object o);

	public enum ConfigOption {
		AID,
		UID,
		HOST,
		PORT,
		URL
	}

	public enum DataField {
		TIMESTAMP(DataType.INT),
		APPVERSIONNUMBER(DataType.INT),
		APPLICATIONMODE(DataType.STRING),
		LOCATIONPROVIDER(DataType.STRING),
		LATITUDE(DataType.DOUBLE),
		LONGITUDE(DataType.DOUBLE),
		POSITIONACCURACY(DataType.DOUBLE),
		SPEED(DataType.DOUBLE),
		HEADING(DataType.DOUBLE),
		COMPASSHEADING(DataType.DOUBLE),
		STEPSPERMINUTE(DataType.DOUBLE),
		USERMOVING(DataType.BOOL),
		SCANNING(DataType.BOOL),
		INHAND(DataType.BOOL),
		TOTALSCANTIME(DataType.INT),
		TOTALINHANDTIME(DataType.INT),
		FOLLOWINGROUTE(DataType.BOOL),
		INTERACTION(DataType.BOOL),
		USERCENTRED(DataType.BOOL),
		MAPROTATED(DataType.BOOL),
		TOTALINTERACTIONTIME(DataType.INT),
		ROUTEAVAILABLE(DataType.BOOL),
		ROUTECONTAINSMORETHANONEWAYPOINT(DataType.BOOL),
		DESTINATIONREACHED(DataType.BOOL),
		TACTILEFEEDBACKENABLED(DataType.BOOL),
		WATCHINGTUTORIAL(DataType.BOOL),
		DISORIENTATION(DataType.BOOL),
		NAVIGATIONERROR(DataType.BOOL),
		ROUTELENGTH(DataType.DOUBLE),
		PROXIMITY(DataType.BOOL),
		LIGHTLEVEL(DataType.DOUBLE),
		TEMPERATURE(DataType.DOUBLE);

		@SuppressWarnings("unused")
		private DataType type;
		DataField(DataType type) {
			this.type = type;
		}
	};
	public enum DataType {
		INT,
		DOUBLE,
		STRING,
		BOOL
 	}
}


