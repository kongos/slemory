package org.haptimap.hcimodules;

import org.haptimap.HaptiMap.HaptiMapCallbackType;

import android.content.Context;


/**
  * The TactileCompass activity takes a a List of
  * waypoints and navigates the user in given order
  * until the last point is reached.
  */
public class TactileCompass extends HCIModule {

	public TactileCompass(Context ctx) {
		super(ctx);
	}

	public native void onStart();
	public native void onPause();
	public native void onResume();
	public native void onStop();
	public native void onDestroy();

	public native int setMode(VectorCount vc);
	public native int setNextWaypoint(Waypoint wp, HaptiMapCallbackType cb);

	public class Waypoint {
		public float lon, lat, alt;

		public Waypoint(float lon, float lat, float alt) {
			this.lon = lon;
			this.lat = lat;
			this.alt = alt;
		}
		public Waypoint(float lon, float lat) {
			this(lon, lat, (float)0.0);
		}
	}

	public enum VectorCount {
		FOUR,
		SIX,
		EIGHT
	}
}
