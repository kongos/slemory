package org.haptimap.hcimodules.scanning;

import java.util.List;

import org.haptimap.hcimodules.HCIModule;
import org.haptimap.hcimodules.util.WayPoint;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

/**
 * This is a simplified {@link HCIModule} based on the {@link ScanOrientation} module which
 * provides speech feedback only.
 * 
 * For more details about the module, please read the {@link ScanOrientation} documentation.
 * 
 * @see ScanOrientation
 * 
 * @author Miguel Molina, October 2011
 */
public class SpeechScanOrientation extends ScanOrientation{

	private static final String TAG = "SpeechScanOrientation";

	/**
	 * Default constructor setting the default values for speech feedback. 
	 *
	 * @param context The context expected is an {@link Activity#getApplicationContext()}
	 * @param distanceEnabled Whether this module should support distance encoding or not.
	 */
	public SpeechScanOrientation(Context context, boolean distanceEnabled) {
		super(context);
		this.setHapticFeedbackEnabled(false);
		this.setSpeechFeedbackEnabled(true);
		this.setDistanceFeedbackEnabled(distanceEnabled);
	}

	/**
	 * Constructor setting the default values for haptic feedback and a list of waypoints.
	 * 
	 * @param context The context expected is an {@link Activity#getApplicationContext()}
	 * @param distanceEnabled Whether this module should support distance encoding or not.
	 * @param list A list containing waypoints
	 */
	public SpeechScanOrientation(Context context, boolean distanceEnabled, List<WayPoint> list){
		this(context, distanceEnabled);
		this.addWayPoints(list);
	}

	/**
	 * <b>This module does not support haptic feedback. If the function is needed, please use
	 * {@link ScanOrientation} or {@link HapticScanOrientation} instead</b>
	 */
	@Override
	public void setHapticFeedbackEnabled(boolean hapticFeedbackEnabled) {
		Log.w(TAG, "This module does not support haptic feedback");
	}

}
