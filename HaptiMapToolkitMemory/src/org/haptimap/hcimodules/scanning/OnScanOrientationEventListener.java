package org.haptimap.hcimodules.scanning;


/**
 * Used for receiving notifications from the {@link ScanOrientation} module when a target has been found. 
 * The method is called if the current listener has been registered with the <code>ScanOrientation</code>
 * module using the <code>setOnScanOrientationEventListener(OnScanOrientationEventListener)</code>
 * method.
 * 
 * @author Miguel Molina, August 2011
 */

public interface OnScanOrientationEventListener {
	
	/**
	 * This event is called when the <code>ScanOrientation</code> finds a target that meets the
	 * requirements given by the user.
	 *  
	 * @param name The name of the Point of interest
	 * @param distance The current distance from the user's position to the Point of interest
	 * @param deviation The current deviation from target
	 */
	public void onTargetFound(String name, int distance, float deviation);
	

}
