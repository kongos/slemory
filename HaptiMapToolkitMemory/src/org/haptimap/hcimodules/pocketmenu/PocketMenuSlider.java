package org.haptimap.hcimodules.pocketmenu;

import android.content.Context;

/**
 * The slider
 * @author <a href="mailto:"martin.pielot@offis.de">Martin Pielot</a>
 * @version Nov 06, 2010
 */
public class PocketMenuSlider extends PocketMenuItem {

	private double	value;
	private boolean immedateCallback;

	public PocketMenuSlider(Context context, String name, int drawableId, boolean immedateCallback) {
		super(context, name, drawableId);
		this.immedateCallback = immedateCallback;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}
	
	public boolean getImmediateCallback() {
		return immedateCallback;
	}

	public String toString() {
		return getName() + " Slider";
	}

	public String getDetailedDescription() {
		return getName()
				+ " "
				+ PocketMenu.toString(value)
				+ ". Swipe left and right to alter";
	}
}
