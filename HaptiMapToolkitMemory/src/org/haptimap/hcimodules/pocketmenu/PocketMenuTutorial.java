package org.haptimap.hcimodules.pocketmenu;

import java.util.ArrayList;
import java.util.List;

import org.haptimap.hcimodules.pocketmenu.PocketMenuTutorialTask.InteractionType;

import android.util.Log;

/**
 * Tutorials
 * @author <a href="mailto:"martin.pielot@offis.de">Martin Pielot</a>
 * @version Nov 07, 2010
 */
public class PocketMenuTutorial {

	// ========================================================================
	// Constant Fields
	// ========================================================================

	private static final String				TAG	= "PocketMenuTutorial";

	// ========================================================================
	// Fields
	// ========================================================================

	protected PocketMenu					pocketMenu;

	// PocketMenu's old configuration.
	private boolean							alwaysVisible;
	private boolean							consumeAllEvents;
	private boolean							speakItemsOnSelection;
	private boolean							speakItemsOnLongClick;
	private boolean							stopSpeechOnTouchUp;

	protected List<PocketMenuTutorialTask>	tasks;
	private int								currentTaskIndex;

	// ========================================================================
	// Constructor
	// ========================================================================

	public PocketMenuTutorial(PocketMenu pocketMenu) {
		this.pocketMenu = pocketMenu;
		this.pocketMenu.setTutorialText("Here we will find the help text");
		this.tasks = new ArrayList<PocketMenuTutorialTask>();
	}

	// ========================================================================
	// Methods
	// ========================================================================

	public void addTask(String text, PocketMenuItem item, InteractionType type) {
		PocketMenuTutorialTask task = new PocketMenuTutorialTask(
				this,
				text,
				item,
				type);
		this.tasks.add(task);
	}

	public void start() {
		Log.i(TAG, "Starting tutorial");

		this.alwaysVisible = pocketMenu.isAlwayVisible();
		this.consumeAllEvents = pocketMenu.isConsumeAllEvents();
		this.speakItemsOnSelection = pocketMenu.isSpeakItemsOnSelection();
		this.speakItemsOnLongClick = pocketMenu.isSpeakItemOnLongClick();
		this.stopSpeechOnTouchUp = pocketMenu.isStopSpeechOnTouchUp();

		this.pocketMenu.setTutorialVisible(true);
		this.pocketMenu.setAlwaysVisible(false);
		this.pocketMenu.setConsumeAllEvents(true);
		this.pocketMenu.setSpeakItemOnLongClick(false);
		this.pocketMenu.setSpeakItemsOnSelection(false);
		this.pocketMenu.setStopSpeechOnTouchUp(false);
		this.currentTaskIndex = 0;
		setupCurrentTask();
	}

	public void onCompleted(PocketMenuTutorialTask task) {
		// Log.i(TAG, "onCompleted( "+task+" )");

		PocketMenuTutorialTask ct = getCurrentTask();
		this.pocketMenu.removeListener(ct);
		
		currentTaskIndex++;
		setupCurrentTask();
	}

	public void onReturnToPrev(PocketMenuTutorialTask task) {
		if (currentTaskIndex > 0) {
			currentTaskIndex--;
		}
		setupCurrentTask();
	}

	// ------------------------------------------------------------------------
	// Helpers
	// ------------------------------------------------------------------------

	protected void setupCurrentTask() {
		PocketMenuTutorialTask currentTask = getCurrentTask();
		Log.i(TAG, "Current task is " + currentTask);
		if (currentTask != null) {
			this.pocketMenu.setTutorialText(currentTask.getTutorialText());
			this.pocketMenu.addListener(currentTask);
			this.pocketMenu
					.setSpeakItemOnLongClick(currentTask.getType() == InteractionType.SLIDER_LONG_CLICK);
		} else {
			this.pocketMenu.setTutorialText("Tutorial Completed");

			// Restore original state of the PocketMenu
			this.pocketMenu.setTutorialVisible(false);
			this.pocketMenu.setAlwaysVisible(alwaysVisible);
			this.pocketMenu.setConsumeAllEvents(consumeAllEvents);
			this.pocketMenu.setSpeakItemsOnSelection(speakItemsOnSelection);
			this.pocketMenu.setSpeakItemOnLongClick(speakItemsOnLongClick);
			this.pocketMenu.setStopSpeechOnTouchUp(stopSpeechOnTouchUp);
		}
		this.pocketMenu.invalidate();
	}

	protected PocketMenuTutorialTask getCurrentTask() {
		try {
			return tasks.get(currentTaskIndex);
		} catch (Exception e) {
			return null;
		}
	}
}
