package org.haptimap.hcimodules.pocketmenu;

import android.content.Context;

/**
 * PocketMenu button
 * @author <a href="mailto:"martin.pielot@offis.de">Martin Pielot</a>
 * @version Nov 06, 2010
 */
public class PocketMenuButton extends PocketMenuItem {

	public PocketMenuButton(Context context, String name, int drawableId) {
		super(context, name, drawableId);
	}

	public String toString() {
		return getName() + " Button";
	}

	public String getDetailedDescription() {
		return getName()
				+ " button. Swipe towards the center to execute "
				+ getName();
	}
}
