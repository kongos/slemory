package org.haptimap.hcimodules.pocketmenu;

import android.util.Log;

/**
 * Single tutorial task.
 * @author <a href="mailto:"martin.pielot@offis.de">Martin Pielot</a>
 * @version Nov 07, 2010
 */
public class PocketMenuTutorialTask implements PocketMenuListener {

	// ========================================================================
	// Enums and Fields
	// ========================================================================

	private static final String	TAG	= "PocketMenuTutorial";

	public enum InteractionType {
		SELECT, INTERACT, SLIDER_CHANGING, SLIDER_CHANGED, SLIDER_LONG_CLICK
	};

	protected PocketMenuTutorial	tutorial;
	protected String				tutorialText;
	protected PocketMenuItem		item;
	protected InteractionType		type;

	// ========================================================================
	// Construction
	// ========================================================================

	public PocketMenuTutorialTask(PocketMenuTutorial tutorial,
			String tutorialText,
			PocketMenuItem item,
			InteractionType type) {
		this.tutorial = tutorial;
		this.tutorialText = tutorialText;
		this.item = item;
		this.type = type;
	}

	// ========================================================================
	// Methods
	// ========================================================================

	public String getTutorialText() {
		return tutorialText;
	}

	public InteractionType getType() {
		return type;
	}

	public String toString() {
		return type + " " + item;
	}

	// ------------------------------------------------------------------------
	// Implementation of 'PocketMenuListener'
	// ------------------------------------------------------------------------

	public void onItemSelected(PocketMenuItem item) {
		Log.d(TAG, "onItemSelected( " + item + " ) - " + type);
		if (this.item == item && type.equals(InteractionType.SELECT)) {
			this.tutorial.onCompleted(this);
		}
	}

	public void onInteractedWithItem(PocketMenuItem item) {
		Log.d(TAG, "onInteractedWithItem( " + item + " ) - " + type);
		if (this.item == item && type.equals(InteractionType.INTERACT)) {
			this.tutorial.onCompleted(this);
		}
	}

	public boolean onSliderChanging(PocketMenuSlider slider, double value) {
		Log.d(TAG, "onSliderChanging( " + slider + " ) - " + type);
		if (item == slider && type.equals(InteractionType.SLIDER_CHANGING)) {
			this.tutorial.onCompleted(this);
		}
		return false;
	}

	public boolean onSliderChanged(PocketMenuSlider slider, double value) {
		Log.d(TAG, "onSliderChanged( " + slider + " ) - " + type);
		if (item == slider && type.equals(InteractionType.SLIDER_CHANGED)) {
			this.tutorial.onCompleted(this);
		}
		return false;
	}

	public String getSliderStateDescription(PocketMenuSlider slider,
			double value) {
		Log.d(TAG, "getSliderStateDescription( " + slider + " )");
		if (item == slider && type.equals(InteractionType.SLIDER_LONG_CLICK)) {
			this.tutorial.onCompleted(this);
		}
		return slider.getDetailedDescription();
	}

}
