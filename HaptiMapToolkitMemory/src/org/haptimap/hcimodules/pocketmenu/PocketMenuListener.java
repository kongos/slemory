/****************************************************************************
 * PocketMenuListener
 ****************************************************************************/
package org.haptimap.hcimodules.pocketmenu;


/**
 * Interface for apps that use the PocketMenu 
 * @author <a href="mailto:"martin.pielot@offis.de">Martin Pielot</a>
 * @version Nov 06, 2010
 */
public interface PocketMenuListener {

	// ========================================================================
	// Method signatures
	// ========================================================================
	

	/**
	 * Called when an item is selected.
	 */
	public void onItemSelected(PocketMenuItem item);

	/**
	 * Called when a button item of the PocketMenu has been selected.
	 */
	public void onInteractedWithItem(PocketMenuItem item);
	
	/**
	 * Called when the value associated with a slider item is being changed at the moment
	 * @param slider the slider object
	 * @param value range [0;1], where 0 = minimum and 1 = maximum
	 * @return true if the slider represents discrete categories and the category has changed
	 */
	public boolean onSliderChanging(PocketMenuSlider slider, double value);

	/**
	 * Called when the slider has been changed and the finger is released.
	 * @param slider the slider object
	 * @param value range [0;1], where 0 = minimum and 1 = maximum
	 * @return true if the slider represents discrete categories and the category has changed
	 */
	public boolean onSliderChanged(PocketMenuSlider slider, double value);
	
	/**
	 * Requests a description of the slider's state.
	 */
	public String getSliderStateDescription(PocketMenuSlider slider, double value);
}
