package org.haptimap.hcimodules.pocketmenu;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/**
 * Abstract class of PocketMenu items.
 * @author <a href="mailto:"martin.pielot@offis.de">Martin Pielot</a>
 *  @version Nov 06, 2010
 */
public abstract class PocketMenuItem {

	private Context		context;
	private String		name;
	private Drawable	icon;
	private long		lastUse;

	public PocketMenuItem(Context context, String name, int drawableId) {
		this.context = context;
		this.name = name;
		this.icon = context.getResources().getDrawable(drawableId);
	}

	public void changeIcon(int drawableId) {
		Rect bounds = icon.getBounds();
		this.icon = context.getResources().getDrawable(drawableId);
		this.icon.setBounds(bounds);
	}

	public Drawable getIcon() {
		return icon;
	}

	public void setBounds(Rect bounds) {
		this.icon.setBounds(bounds);
	}

	public Rect getBounds() {
		return icon.getBounds();
	}

	public Point getCenter() {
		Point p = new Point();
		Rect bounds = icon.getBounds();
		p.x = (bounds.right - bounds.left) / 2;
		p.y = (bounds.bottom - bounds.top) / 2;
		return p;
	}

	public boolean contains(int x, int y) {
		return this.getBounds().contains(x, y);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastUse(long use) {
		lastUse = use;
	}

	public long getLastUse() {
		return lastUse;
	}

	public abstract String getDetailedDescription();
}
