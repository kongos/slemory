package org.haptimap.hcimodules.util;

import java.util.List;

import org.haptimap.hcimodules.HCIModule;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.View;

public class MagneticCompass extends HCIModule implements SensorEventListener{

	private static final String TAG = "Compass";
	private static final boolean DEBUG = true;
	private SensorManager mSensorManager;
	private float[] accelerometerValues;
	private float[] magneticFieldValues;
	private boolean sensorsRegistered;
	private static float[] orientationValues = new float[3];
	private static float[] R = new float[9];
	private boolean sensorsStable;
	private static float[] prevValues = new float[3];
	private CompassListener mCompassListener;

	public MagneticCompass(Context context){
		super(context);
		this.accelerometerValues = new float[3];
		this.magneticFieldValues = new float[3];
		this.sensorsRegistered = false;
		this.sensorsStable = false;
	}

	@Override
	public void onStart() {
		if(!sensorsRegistered){
			sensorsRegistered = registerSensors();
		}
	}

	private boolean registerSensors(){
		mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		boolean accEnabled = false;
		boolean magEnabled = false;

		List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);

		for(Sensor sensor : sensorList) {
			if(DEBUG) Log.d(TAG, "Sensor in list is: " + sensor.getName());

			switch(sensor.getType()){

			case Sensor.TYPE_ACCELEROMETER:
				accEnabled = mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
				break;

			case Sensor.TYPE_MAGNETIC_FIELD:
				magEnabled = mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
				break;

			default:
				break;
			}
		}
		return (accEnabled && magEnabled);
	}

	@Override
	public void onPause() {
		sensorsRegistered = unregisterSensors();
	}

	@Override
	public void onResume() {
		if(!sensorsRegistered){
			sensorsRegistered = registerSensors();
		}
	}

	@Override
	public void onStop() {
		sensorsRegistered = unregisterSensors();		
	}


	@Override
	public void onDestroy() {
		if(sensorsRegistered){
			sensorsRegistered = unregisterSensors();
		}
		mSensorManager = null;		
	}

	private boolean unregisterSensors() {
		boolean ans = false;
		try{
			List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);
			for(Sensor sensor : sensorList) {
				mSensorManager.unregisterListener(this, mSensorManager.getSensorList(sensor.getType()).get(0));
				if(DEBUG) Log.d(TAG, "Sensor " + sensor.getName() + " unregistered");
			}
		}catch (Exception e){
			Log.e(TAG, "Exception when unloading sensors " + e);
			ans = true;
		}		
		if(DEBUG) Log.d(TAG, "ALL sensors unregistered");
		return ans;
	}

	private float[] calculateOrientation(float[] accelerometerValues, float[] magneticFieldValues) {
		if(sensorsStable){
			SensorManager.getRotationMatrix(R, null, accelerometerValues, magneticFieldValues);
			SensorManager.getOrientation(R, orientationValues);
			orientationValues[0] = (float) Math.toDegrees(orientationValues[0]);
			orientationValues[1] = (float) Math.toDegrees(orientationValues[1]);
			orientationValues[2] = (float) Math.toDegrees(orientationValues[2]);
			prevValues = orientationValues.clone();
			return orientationValues;
		} else {
			return prevValues;
		}
	}

	
	public void onSensorChanged(SensorEvent event) {

		switch(event.sensor.getType()){

		case Sensor.TYPE_ACCELEROMETER:
			accelerometerValues = event.values.clone();
			break;

		case Sensor.TYPE_MAGNETIC_FIELD:
			magneticFieldValues = event.values.clone();
			break;

		default:
			break;
		}

		if(mCompassListener != null){
			mCompassListener.onCompassChanged(getOrientationValues());
		}
	}

	/**
	 * Calculates the current orientation of the device based on 
	 * accelerometer's and the magnetic fields sensor's orientationValues .
	 * @return 
	 * <li>orientationValues[0]: azimuth, rotation around the Z axis.
	 * <li>orientationValues[1]: pitch, rotation around the X axis.
	 * <li>orientationValues[2]: roll, rotation around the Y axis. 
	 */
	public float[] getOrientationValues() throws IllegalStateException{
		return calculateOrientation(accelerometerValues, magneticFieldValues);
	} 	

	public void onAccuracyChanged(Sensor sensor, int accuracy) {

		switch(accuracy){

		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			sensorsStable = true;
			break;

		default:
			sensorsStable = false;
			if(DEBUG) Log.d(TAG, sensor.getName() + " sensorsStable = false");
			break;			
		}
	}


	public interface CompassListener{

		public void onCompassChanged(float[] values);
	}

	public void setOnCompassListener(CompassListener listener){
		this.mCompassListener = listener;
	}

	public void unregisterListener(){
		if(this.mCompassListener == null){
			throw new IllegalArgumentException("There are no sensors registered");
		}
		this.mCompassListener = null;
	}

	public CompassView getCompassView(Context context){
		return new CompassView(context);
	}

	public class CompassView extends View {
		private Paint paint = new Paint();
		private Bitmap arrow;
		protected float mAzimuth;
		private boolean externalValue;

		public CompassView(Context context) {
			super(context);
			arrow = BitmapFactory.decodeResource(context.getResources(),
				context.getResources().getIdentifier("org.haptimap:drawable/arrow", null, null));
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			canvas.drawColor(Color.WHITE);

			paint.setAntiAlias(true);
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.FILL);

			int w = canvas.getWidth() - arrow.getWidth();
			int h = canvas.getHeight() - arrow.getHeight();
			int cx = w / 2;
			int cy = h / 2;

			canvas.save();
			canvas.translate(cx, cy);
			
			canvas.rotate(externalValue ? this.mAzimuth : -orientationValues[0], arrow.getWidth()/2, arrow.getHeight()/2);
			canvas.drawBitmap(arrow, 0, 0, paint);
			canvas.restore();
		}
		
		public void setDeviationValue(float value){
			this.mAzimuth = value;
			externalValue = true;
		}
		
		public void unsetExternalValue(){
			this.externalValue = false;
		}
	}

}
