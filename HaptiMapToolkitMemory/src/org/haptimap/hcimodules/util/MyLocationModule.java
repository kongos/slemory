package org.haptimap.hcimodules.util;

import org.haptimap.hcimodules.HCIModule;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class MyLocationModule extends HCIModule implements LocationListener{

	private LocationManager mLocationManager;
	private MyLocationListener mMyLocationListener;
	private Location currentLocation;
	private boolean updating;
	private String mProvider;

	private static final int ON_LOCATION_CHANGED = 1;

	public MyLocationModule(Context context) {
		super(context);
		mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		mProvider = LocationManager.GPS_PROVIDER;
	}

	@Override
	public void onStart() {
		if(!updating){
			mLocationManager.requestLocationUpdates(mProvider, 0, 0, this);	
			updating = true;
		}
	}

	@Override
	public void onPause() {
		if(updating){
			mLocationManager.removeUpdates(this);
			updating = false;
		}
	}

	@Override
	public void onResume() {
		if(!updating){
			if(mLocationManager != null){
				mLocationManager.requestLocationUpdates(mProvider, 0, 0, this);
			} else {
				mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
				mLocationManager.requestLocationUpdates(mProvider, 0, 0, this);
			}
			updating = true;
		} else {
			return;
		}
	}

	@Override
	public void onStop() {
		mLocationManager.removeUpdates(this);
		updating = false;
	}

	@Override
	public void onDestroy() {
		mLocationManager.removeUpdates(this);
		mLocationManager = null;
		mMyLocationListener = null;
		updating = false;
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		currentLocation = location;
		if(mMyLocationListener != null){
			//			mMyLocationListener.onMyLocationChanged(location);
			Message msg = Message.obtain();
			msg.what = ON_LOCATION_CHANGED;
			msg.obj = location;
			mHandler.sendMessage(msg);
		}
	}

	
	/**
	 * These values can be retrieved from {@link LocationManager} providers' constants
	 * @param provider could be "gps", "network" or "passive".
	 * @return whether the provider was changed.
	 * 
	 * @see LocationManager
	 */
	public boolean setProvider(String provider){
		boolean ans = false;
		if(LocationManager.GPS_PROVIDER.equals(provider)){
			mProvider = provider;
			ans = true;
		} else if (LocationManager.NETWORK_PROVIDER.equals(provider)){
			mProvider = provider;
			ans = true;
		} else {
			ans = false;
		}
		if(ans){
			onStop();
			onStart();
		}
		return ans;
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public Location getCurrentLocation(){
		return currentLocation;
	}

	public void setOnMyLocationListener(MyLocationListener listener){
		mMyLocationListener = listener;
	}

	public void unregisterListener(){
		mMyLocationListener = null;
	}

	public interface MyLocationListener{

		public void onMyLocationChanged(Location location);
	}

	private Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){

			case ON_LOCATION_CHANGED:
				mMyLocationListener.onMyLocationChanged((Location) msg.obj);
				break;

			default:

				break;
			}
		}

	};
}