#include <jni.h>

#include "haptimap_android_jni.h"
#include "common.h"

/*
 * Class:     org_haptimap_HaptiMap_HaptiMapGLRenderer
 * Method:    init_callback
 * Signature: ()V
 */
void Java_org_haptimap_HaptiMap_00024HaptiMapGLRenderer_init_1callback
  (JNIEnv *env, jobject renderer)
{
    hm_android_visual_display_init_callback(env, renderer);
}

/*
 * Class:     org_haptimap_HaptiMap_HaptiMapGLRenderer
 * Method:    render_callback
 * Signature: (II)V
 */
void Java_org_haptimap_HaptiMap_00024HaptiMapGLRenderer_render_1callback
  (JNIEnv *env, jobject renderer, jint w, jint h)
{
    hm_android_visual_display_render_callback(env, renderer, w, h);
}
