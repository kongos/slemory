/*
	The previously used macros only work inside HaptiMap core modules. But we probably
	want to use them elsewhere as well, like in the hcimodules.
	hm_jni_*_long is a possible solution for the problem. We use an android provided
	service to save data inside the application context which in turn can be accessed 
	by all activities.
*/
#define SET_HM(C,H) (hm_jni_save_long(C, "hm", (long)H))
#define GET_HM(C) ((hm_t *)(hm_jni_get_long(C, "hm")))

/* Macro to get the ordinal value of a Java enum. If the enums are defined identically
 * in HaptiMap.java as in haptimap_types.h, then fingers crossed this will be the same 
 * integer value as for the corresponding C enum---it is admittedly a bit fragile though. */
#define GET_ORDINAL(E) (*env)->CallIntMethod(env, E, (*env)->GetMethodID(env, \
                                                       (*env)->GetObjectClass(env, E), "ordinal", "()I"))
/* Macro to store a single integer in a 1-element Java array. We do this to simulate
 * the pointer-to-integer method of returning single integers in the toolkit C API. */
#define STORE_INT(P,I) (*env)->SetIntArrayRegion(env, (P), 0, 1, &(I));

/* Macros to map and unmap a Java integer array to/from a C integer array to
 * allow it to be modified by C functions. */
#define MAP_INT(P,I) (I = (*env)->GetIntArrayElements(env, (P), JNI_FALSE))
#define UNMAP_INT(P,I) (*env)->ReleaseIntArrayElements(env, (P), (I), JNI_FALSE)

void hm_jni_callback_wrapper(void *);
void hm_jni_save_long(jobject ctx, char *key, long value);
long hm_jni_get_long(jobject ctx, char *key);
