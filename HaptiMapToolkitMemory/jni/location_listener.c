#include <jni.h>

#include "haptimap_android_jni.h"

/*
 * Class:     org_haptimap_HaptiMap_HaptiMapLocationListener
 * Method:    onLocationChanged
 * Signature: (Landroid/location/Location;)V
 */
void Java_org_haptimap_HaptiMap_00024HaptiMapLocationListener_onLocationChanged
  (JNIEnv *env, jobject listener, jobject location)
{
    hm_android_location_sensor_onLocationChanged(env, listener, location);
}

/*
 * Class:     org_haptimap_HaptiMap_HaptiMapLocationListener
 * Method:    onStatusChanged
 * Signature: (Ljava/lang/String;ILandroid/os/Bundle;)V
 */
void Java_org_haptimap_HaptiMap_00024HaptiMapLocationListener_onStatusChanged
  (JNIEnv *env, jobject listener, jstring prov_name, jint status, jobject extras)
{
    hm_android_location_sensor_onStatusChanged(env, listener, prov_name, status, extras);
}

/*
 * Class:     org_haptimap_HaptiMap_HaptiMapLocationListener
 * Method:    onProviderDisabled
 * Signature: (Ljava/lang/String;)V
 */
void Java_org_haptimap_HaptiMap_00024HaptiMapLocationListener_onProviderDisabled
  (JNIEnv *env, jobject listener, jstring prov_name)
{
    hm_android_location_sensor_onProviderDisabled(env, listener, prov_name);
}

/*
 * Class:     org_haptimap_HaptiMap_HaptiMapLocationListener
 * Method:    onProviderEnabled
 * Signature: (Ljava/lang/String;)V
 */
void Java_org_haptimap_HaptiMap_00024HaptiMapLocationListener_onProviderEnabled
  (JNIEnv *env, jobject listener, jstring prov_name)
{
    hm_android_location_sensor_onProviderEnabled(env, listener, prov_name);
}
