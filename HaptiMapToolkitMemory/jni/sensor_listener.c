#include <jni.h>

#include "common.h"
#include "haptimap.h"
#include "haptimap_android2.h"

void Java_org_haptimap_HaptiMap_00024HaptiMapSensorListener_onAccuracyChanged(JNIEnv *env, jobject thiz, jobject sensor, jint accuracy) {
	hm_android_onAccuracyChanged(env, thiz, sensor, accuracy);
}
void Java_org_haptimap_HaptiMap_00024HaptiMapSensorListener_onSensorChanged(JNIEnv *env, jobject thiz, jobject sensorevent) {	
	hm_android_onSensorChanged(env, thiz, sensorevent);
}
