#
# Android NDK Makefile for HaptiMap toolkit libary
#
# Toolkit core, mantle and plugins are compiled as 3 separate modules
# into static libraries, which are then linked together along with some
# local JNI wrapper code into one shared library.
#
# Note that the NDK build system will only include functions from the
# static libaries into the final shared library if they are actually
# used by the JNI wrapper layer. All other functions are pruned.
#

LOCAL_PATH := $(call my-dir)
MY_TOP_DIR := $(LOCAL_PATH)/../../../..


include $(CLEAR_VARS)
LOCAL_MODULE := haptimap

LOCAL_C_INCLUDES := $(MY_TOP_DIR)/include/public $(MY_TOP_DIR)/include/private \
      $(MY_TOP_DIR)/core/android2 $(MY_TOP_DIR)/mantle
LOCAL_SRC_FILES := jni.c init.c geodata.c plugins.c location_sensor.c \
      location_listener.c renderer.c virtual_observer.c tactile_compass.c \
		sensor_listener.c sensor.c

# The JNI interface code stores a pointer to the toolkit internal state
# as a long integer in the Java wrapper class. The casts to and from
# this integer cause a lot of harmless warnings (we are storing a 32-bit
# pointer in a 64-bit integer), which the following flags suppress.
LOCAL_CFLAGS := -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast -O0

# Note ordering of libraries; dependent libraries must be mentioned
# before the library they depend on, to ensure that the functions they
# need are not pruned.
LOCAL_STATIC_LIBRARIES := haptimap-mantle haptimap-plugins haptimap-core haptimap-plugins
LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)


# libhaptimap-plugins.a
include $(CLEAR_VARS)
include $(MY_TOP_DIR)/plugins/Android.mk

# libhaptimap-core.a
include $(CLEAR_VARS)
include $(MY_TOP_DIR)/core/Android.mk

# libhaptimap-mantle.a
include $(CLEAR_VARS)
include $(MY_TOP_DIR)/mantle/Android.mk
