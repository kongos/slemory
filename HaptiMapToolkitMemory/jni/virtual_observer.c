#include <jni.h>
#include "virtualobserver.h"
#include "common.h"

jclass data_field_class;
jfieldID data_field_type;


/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    onStart
 * Signature: ()V
 */
void Java_org_haptimap_hcimodules_VirtualObserver_onStart(JNIEnv *env, jobject thiz) {
	data_field_class = (*env)->FindClass(env, "org/haptimap/hcimodules/VirtualObserver/DataField");
	data_field_type = (*env)->GetFieldID(env, data_field_class, "type", "Lorg/haptimap/hcimodules/VirtualObserver/DataType");	
}
/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    onPause
 * Signature: ()V
 */
void Java_org_haptimap_hcimodules_VirtualObserver_onPause(JNIEnv *env, jobject thiz) {

}
/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    onResume
 * Signature: ()V
 */
void Java_org_haptimap_hcimodules_VirtualObserver_onResume(JNIEnv *env, jobject thiz) {

}
/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    onStop
 * Signature: ()V
 */
void Java_org_haptimap_hcimodules_VirtualObserver_onStop(JNIEnv *env, jobject thiz) {

}
/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    onDestroy
 * Signature: ()V
 */
void Java_org_haptimap_hcimodules_VirtualObserver_onDestroy(JNIEnv *env, jobject thiz) {

}
/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    setDataField
 * Signature: (Lorg/haptimap/hcimodules/VirtualObserver/DataField;Ljava/lang/Object;)I
 */
jint Java_org_haptimap_hcimodules_VirtualObserver_setDataField(JNIEnv *env, jobject thiz, jobject datafield, jobject jdata) {
	int field;
	jint i;
	jdouble d;
	jboolean b;
	char *str;

	if(vo_t.data == NULL)
		return HM_FAILURE;

	field = GET_ORDINAL(datafield);
	
	if(field >= 0 && field <= 32) {
		enum DATA_FIELDS f = field;
		enum DATA_TYPE t = GET_ORDINAL((*env)->GetObjectField(env, data_field_class, data_field_type));

		switch(t) {
			case INT:
				i = (*env)->CallIntMethod(env, jdata, 
					(*env)->GetMethodID(env, (*env)->FindClass(env, "java/lang/Integer"), "intValue", "()I"));
				return vo_set_value(&vo_t.data, f, t, &i, 0);
			case DOUBLE:
				d = (*env)->CallDoubleMethod(env, jdata, 
					(*env)->GetMethodID(env, (*env)->FindClass(env, "java/lang/Double"), "doubleValue", "()D"));
				return vo_set_value(&vo_t.data, f, t, &d, 0);
			case STRING:
				str = (char *)(*env)->GetStringUTFChars(env, (jstring)jdata, NULL);
				return vo_set_value(&vo_t.data, f, t, str, strlen(str));
				break;
			case BOOL:
				b = (*env)->CallBooleanMethod(env, jdata, 
					(*env)->GetMethodID(env, (*env)->FindClass(env, "java/lang/Boolean"), "booleanValue", "()Z"));
				return vo_set_value(&vo_t.data, f, t, &b, 0);
				break;
		}
	}
	else
		return HM_FAILURE;
}
/*
 * Class:     org_haptimap_hcimodules_VirtualObserver
 * Method:    setConfigOption
 * Signature: (Lorg/haptimap/hcimodules/VirtualObserver/ConfigOption;Ljava/lang/Object;)I
 */
jint Java_org_haptimap_hcimodules_VirtualObserver_setConfigOption(JNIEnv *env, jobject thiz, jobject option, jobject jdata) {
	int o = GET_ORDINAL(option);
	jstring jstr;
	jshort jshrt;
	char *str;

	if(o >= 0 && o <= 4) {
		switch(o) {
			case 0: //AID
				jstr = jdata;
				str = (char *)(*env)->GetStringUTFChars(env, jstr, NULL);
				if(vo_t.aid != NULL)
					free(vo_t.aid);
				vo_t.aid = str;
				break;
			case 1: //UID
				jstr = jdata;
				str = (char *)(*env)->GetStringUTFChars(env, jstr, NULL);
				if(vo_t.uid != NULL)
					free(vo_t.uid);
				vo_t.uid = str;
				break;
			case 2: //HOST
				jstr = jdata;
				str = (char *)(*env)->GetStringUTFChars(env, jstr, NULL);
				if(vo_t.host != NULL)
					free(vo_t.host);
				vo_t.host = str;
				break;
			case 3: //PORT
				jshrt	= (*env)->CallShortMethod(env, jdata, 
					(*env)->GetMethodID(env, (*env)->FindClass(env, "java/lang/Integer"), "shortValue", "()S"));
				vo_t.port = jshrt;
				break;
			case 4: //URL
				jstr = jdata;
				str = (char *)(*env)->GetStringUTFChars(env, jstr, NULL);
				if(vo_t.url != NULL)
					free(vo_t.url);
				vo_t.url = str;
				break;
		}
	}
	else
		return HM_FAILURE;
	return HM_SUCCESS;
}
