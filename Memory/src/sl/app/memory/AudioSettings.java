package sl.app.memory;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.SeekBar;

public class AudioSettings extends Activity {
	private AudioManager audioManager;

	/** Called when the activity is first created. */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// content set to ../res/layout/settings.xml

		setContentView(R.layout.settings);
		// get audiomanager, set its maxbolume and fetch currentvolume, put this
		// info in seekbar volcontrol, connect setonseekbarchangedlistener to
		// volcontrol and set so the new value gets passed to the music_volume
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int maxVolume = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		int curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		SeekBar volControl = (SeekBar) findViewById(R.id.seekBar1);
		volControl.setMax(maxVolume);
		volControl.setProgress(curVolume);
		volControl
				.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
					public void onStopTrackingTouch(SeekBar arg0) {
					}

					public void onStartTrackingTouch(SeekBar arg0) {
					}

					public void onProgressChanged(SeekBar arg0, int arg1,
							boolean arg2) {
						audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
								arg1, 0);
					}
				});

	}

	protected void onStart() {
		super.onStart();

	}

	protected void onResume() {

		super.onResume();

	}

	protected void onPause() {

		super.onPause();

	}

}