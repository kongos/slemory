package sl.app.memory;

import android.app.Activity;
import android.os.Bundle;

public class Help extends Activity {

	/** Called when the activity is first created. */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// content set to ../res/layout/help.xml

		setContentView(R.layout.help);

	}

	protected void onStart() {
		super.onStart();

	}

	protected void onResume() {

		super.onResume();

	}

	protected void onPause() {

		super.onPause();

	}

}