package sl.app.memory;

import org.haptimap.hcimodules.gesture.ShakeNotifier;
import org.haptimap.hcimodules.gesture.ShakeNotifierListener;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class Start extends Activity implements ShakeNotifierListener {
	/** Called when the activity is first created. */

	private ShakeNotifier mShakeNotifier;
	private MediaPlayer djungle;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// content set to ../res/layout/about.xml

		setContentView(R.layout.start);
		// start startup sound, start listening for shakes
		djungle = MediaPlayer.create(this, R.raw.djungel);
		mShakeNotifier = new ShakeNotifier(getApplicationContext());
		mShakeNotifier.setOnShakeListener(this);

	}

	protected void onStart() {
		super.onStart();
		// start listening for shakes and start startup sound
		mShakeNotifier.setModeToShake();
		djungle.start();
		mShakeNotifier.onStart();
	}

	protected void onResume() {
		// start listening for shakes and start startup sound

		super.onResume();
		mShakeNotifier.setModeToShake();
		djungle.start();
		mShakeNotifier.onStart();
	}

	protected void onPause() {
		// pause listening for shakes and pause startup sound

		super.onPause();
		djungle.pause();
		mShakeNotifier.onStop();
	}

	public void onShakeDetected(double force) {
		// stop listening for shakes and create an intent for Game, start Game
		mShakeNotifier.onStop();
		Intent next = new Intent(Start.this, Game.class);
		startActivity(next);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// menuinflater inflated with ../res/layout/menu.xml
		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.layout.menu, menu);

		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// start corresponding activity. Help, AudioSettings or About
		switch (item.getItemId()) {
		case R.id.icontext1:
			Intent next = new Intent(Start.this, Help.class);
			startActivity(next);
			return true;

		case R.id.icontext2:
			Intent next2 = new Intent(Start.this, AudioSettings.class);
			startActivity(next2);
			return true;

		case R.id.icontext3:
			Intent next3 = new Intent(Start.this, About.class);
			startActivity(next3);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void onTurnDetected() {

	}

	public void allowTurn() {

	}

	public void disallowTurn() {

	}

	public void moveLeft() {

	}

	public void moveRight() {

	}

	public void moveUp() {

	}

	public void moveDown() {

	}

}