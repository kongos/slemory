package sl.app.memory;

import android.app.Activity;
import android.os.Bundle;

public class About extends Activity {

	/** Called when the activity is first created. */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// content set to ../res/layout/about.xml
		setContentView(R.layout.about);

	}

	protected void onStart() {
		super.onStart();

	}

	protected void onResume() {

		super.onResume();

	}

	protected void onPause() {

		super.onPause();

	}

}