package sl.app.memory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import org.haptimap.hcimodules.gesture.ShakeNotifier;
import org.haptimap.hcimodules.gesture.ShakeNotifierListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;

public class Game extends Activity implements OnTouchListener,
		ShakeNotifierListener {
	/** Called when the activity is first created. */

	private ImageButton koButton;
	private ImageButton apaButton;
	private ImageButton apaTextButton;
	private ImageButton elefantButton;
	private ImageButton elefantTextButton;
	private ImageButton farButton;
	private ImageButton farTextButton;
	private ImageButton grisButton;
	private ImageButton grisTextButton;
	private ImageButton krokodilButton;
	private ImageButton krokodilTextButton;
	private ImageButton lejonButton;
	private ImageButton lejonTextButton;
	private ImageButton ormButton;
	private ImageButton ormTextButton;
	private ImageButton koTextButton;
	private ImageButton krabbButton;
	private ImageButton krabbTextButton;
	private ImageButton pressedButton;
	private ImageButton firstMatchButton;
	private ImageButton secondMatchButton;

	private ImageButton[] buttonList;
	private ArrayList<ImageButton> allowedList;

	private Boolean readyToRemove;
	private Boolean remove;
	private int selectedBricks;
	private int bricksLeft;
	private int count;
	private int col;
	private int row;
	private ShakeNotifier mShakeNotifier;
	private Vector<Integer> ids;
	private MediaPlayer player;
	private Bundle savedInstanceState;
	Vibrator v;

	public void onCreate(Bundle savedInstanceState) {
		this.savedInstanceState = savedInstanceState;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		buttonList = new ImageButton[18];
		allowedList = new ArrayList<ImageButton>();
		ids = new Vector<Integer>();
		ids.add(R.id.imageButton1);
		ids.add(R.id.imageButton2);
		ids.add(R.id.imageButton3);
		ids.add(R.id.imageButton4);
		ids.add(R.id.imageButton5);
		ids.add(R.id.imageButton6);
		ids.add(R.id.imageButton7);
		ids.add(R.id.imageButton8);
		ids.add(R.id.imageButton9);
		ids.add(R.id.imageButton10);
		ids.add(R.id.imageButton11);
		ids.add(R.id.imageButton12);
		ids.add(R.id.imageButton13);
		ids.add(R.id.imageButton14);
		ids.add(R.id.imageButton15);
		ids.add(R.id.imageButton16);
		ids.add(R.id.imageButton17);
		ids.add(R.id.imageButton18);
		v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		mShakeNotifier = new ShakeNotifier(getApplicationContext());
		mShakeNotifier.setOnShakeListener(this);
		mShakeNotifier.onStart();

		selectedBricks = 0;
		count = 0;
		bricksLeft = 18;

		remove = false;
		readyToRemove = false;

		col = 0;
		row = 0;
		createMemory();

	}

	protected void onStart() {
		super.onStart();

	}

	// initialize a menu with information from ../res/menugame.xml
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.layout.menugame, menu);

		return true;

	}

	// returns the first imagebutton from the buttonlist vector, this button is
	// the one that gets selected when the game is started
	protected ImageButton getFirst() {

		ImageButton returned = null;
		for (int i = 0; i < 18; i++) {

			if (buttonList[i].getId() == R.id.imageButton1) {
				returned = buttonList[i];
			}

		}
		return returned;

	}

	protected void onResume() {
		mShakeNotifier.onStart();
		super.onResume();

	}

	protected void onPause() {
		mShakeNotifier.onStop();
		super.onPause();

	}

	// mShakeNotifier to allowturn assigns each brick with a picture or text,
	// shuffles them and sets
	// background to default on every brick
	private void createMemory() {

		mShakeNotifier.setModeToTurn();
		mShakeNotifier.allowTurn();
		Collections.shuffle(ids);
		koButton = (ImageButton) this.findViewById(ids.get(1));
		koTextButton = (ImageButton) this.findViewById(ids.get(2));
		krabbButton = (ImageButton) this.findViewById(ids.get(5));
		krabbTextButton = (ImageButton) this.findViewById(ids.get(3));
		apaButton = (ImageButton) this.findViewById(ids.get(13));
		apaTextButton = (ImageButton) this.findViewById(ids.get(8));
		elefantButton = (ImageButton) this.findViewById(ids.get(9));
		elefantTextButton = (ImageButton) this.findViewById(ids.get(12));
		farButton = (ImageButton) this.findViewById(ids.get(14));
		farTextButton = (ImageButton) this.findViewById(ids.get(11));
		grisButton = (ImageButton) this.findViewById(ids.get(10));
		grisTextButton = (ImageButton) this.findViewById(ids.get(4));
		krokodilButton = (ImageButton) this.findViewById(ids.get(7));
		krokodilTextButton = (ImageButton) this.findViewById(ids.get(6));
		lejonButton = (ImageButton) this.findViewById(ids.get(16));
		lejonTextButton = (ImageButton) this.findViewById(ids.get(15));
		ormButton = (ImageButton) this.findViewById(ids.get(0));
		ormTextButton = (ImageButton) this.findViewById(ids.get(17));

		addButtonsToList();
		for (int i = 0; i < 18; i++) {
			ImageButton temp = buttonList[i];
			temp.setOnTouchListener(this);
			temp.setImageResource(R.drawable.backgroundmemory);
		}
		pressedButton = getFirst();
		pressedButton.setImageResource(R.drawable.backgroundmemory2);
	}

	// gets called when a turn is detected, and handels the bricks
	public void onTurnDetected() {

		mShakeNotifier.disallowTurn();
		if (remove) {
			count++;

			remove = false;
			if (rightMatch()) {
				bricksLeft = bricksLeft - 2;
				firstMatchButton.setImageResource(R.drawable.black);
				secondMatchButton.setImageResource(R.drawable.black);
				if (bricksLeft == 0) {
					gameOver();
				}

			} else {

				allowedList.add(firstMatchButton);
				allowedList.add(secondMatchButton);
				firstMatchButton.setImageResource(R.drawable.backgroundmemory);
				secondMatchButton.setImageResource(R.drawable.backgroundmemory);

			}
			if (bricksLeft != 0) {
				mShakeNotifier.setSuperAllowed();
				pressedButton = selectNext();
				pressedButton.setImageResource(R.drawable.backgroundmemory2);
			}
		}

		else if (selectedBricks < 2 && pressedButton != null && bricksLeft != 0) {

			setOriginalImage();
			selectedBricks++;
			if (selectedBricks == 1) {

				firstMatchButton = pressedButton;
				allowedList.remove(firstMatchButton);
				pressedButton = selectNext();
				pressedButton.setImageResource(R.drawable.backgroundmemory2);

			} else {

				selectedBricks = 0;
				secondMatchButton = pressedButton;
				allowedList.remove(secondMatchButton);
				readyToRemove = true;
				mShakeNotifier.pressed();
				mShakeNotifier.unSetSuperAllowed();
			}
		}

	}

	// gets called when all bricks have been removed then prompts a dialog to
	// the user
	private void gameOver() {

		for (int i = 0; i < 18; i++) {
			ImageButton temp = buttonList[i];
			pressedButton = temp;
			setOriginalImage();
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Grattis! Du klarade det på " + count + " försök!");
		builder.setCancelable(false);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Game.this.finish();
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	// returns the next brick that is available for selection
	private ImageButton selectNext() {

		ImageButton temp = allowedList.get(allowedList.size() - 1);
		int tempId = temp.getId();
		if (tempId == R.id.imageButton1 || tempId == R.id.imageButton4
				|| tempId == R.id.imageButton7 || tempId == R.id.imageButton10
				|| tempId == R.id.imageButton13 || tempId == R.id.imageButton16) {
			col = 0;

		} else if (tempId == R.id.imageButton2 || tempId == R.id.imageButton5
				|| tempId == R.id.imageButton8 || tempId == R.id.imageButton11
				|| tempId == R.id.imageButton14 || tempId == R.id.imageButton17) {
			col = 1;

		} else {
			col = 2;

		}

		if (tempId == R.id.imageButton1 || tempId == R.id.imageButton2
				|| tempId == R.id.imageButton3) {
			row = 0;
		} else if (tempId == R.id.imageButton4 || tempId == R.id.imageButton5
				|| tempId == R.id.imageButton6) {
			row = 1;

		} else if (tempId == R.id.imageButton7 || tempId == R.id.imageButton8
				|| tempId == R.id.imageButton9) {
			row = 2;

		} else if (tempId == R.id.imageButton10 || tempId == R.id.imageButton11
				|| tempId == R.id.imageButton12) {
			row = 3;

		} else if (tempId == R.id.imageButton13 || tempId == R.id.imageButton14
				|| tempId == R.id.imageButton15) {
			row = 4;
		} else {
			row = 5;
		}
		return temp;
	}

	// called when a brick is turned and sets its correct picture, and if it is
	// a text plays the animal sound
	public void setOriginalImage() {
		if (pressedButton == koButton) {
			koButton.setImageResource(R.drawable.ko);

		} else if (pressedButton == koTextButton) {
			koTextButton.setImageResource(R.drawable.kotext);
			player = MediaPlayer.create(this, R.raw.cowny);
			player.start();
		} else if (pressedButton == krabbButton) {
			krabbButton.setImageResource(R.drawable.krabba);

		} else if (pressedButton == krabbTextButton) {
			krabbTextButton.setImageResource(R.drawable.krabbatext);
			player = MediaPlayer.create(this, R.raw.krabba);
			player.start();
		} else if (pressedButton == apaButton) {
			apaButton.setImageResource(R.drawable.apa);

		} else if (pressedButton == apaTextButton) {
			apaTextButton.setImageResource(R.drawable.apatext);
			player = MediaPlayer.create(this, R.raw.apa);
			player.start();
		} else if (pressedButton == elefantButton) {
			elefantButton.setImageResource(R.drawable.elefant);

		} else if (pressedButton == elefantTextButton) {
			elefantTextButton.setImageResource(R.drawable.elefanttext);
			player = MediaPlayer.create(this, R.raw.elefant);
			player.start();
		} else if (pressedButton == farButton) {
			farButton.setImageResource(R.drawable.far);

		} else if (pressedButton == farTextButton) {
			farTextButton.setImageResource(R.drawable.fartext);
			player = MediaPlayer.create(this, R.raw.far);
			player.start();
		} else if (pressedButton == grisButton) {
			grisButton.setImageResource(R.drawable.gris);

		} else if (pressedButton == grisTextButton) {
			grisTextButton.setImageResource(R.drawable.gristext);
			player = MediaPlayer.create(this, R.raw.gris);
			player.start();
		} else if (pressedButton == krokodilButton) {
			krokodilButton.setImageResource(R.drawable.krokodil);

		} else if (pressedButton == krokodilTextButton) {
			krokodilTextButton.setImageResource(R.drawable.krokodiltext);
			player = MediaPlayer.create(this, R.raw.krokodil);
			player.start();
		} else if (pressedButton == lejonButton) {
			lejonButton.setImageResource(R.drawable.lejon);

		} else if (pressedButton == lejonTextButton) {
			lejonTextButton.setImageResource(R.drawable.lejontext);
			player = MediaPlayer.create(this, R.raw.lejon);
			player.start();
		} else if (pressedButton == ormButton) {
			ormButton.setImageResource(R.drawable.orm);

		} else if (pressedButton == ormTextButton) {
			ormTextButton.setImageResource(R.drawable.ormtext);
			player = MediaPlayer.create(this, R.raw.orm);
			player.start();
		}
	}

	// checks if the two turned bricks are a correct pair
	public boolean rightMatch() {

		if (firstMatchButton == koButton || secondMatchButton == koButton) {
			if (firstMatchButton == koTextButton
					|| secondMatchButton == koTextButton) {
				return true;
			}
		} else if (firstMatchButton == krabbButton
				|| secondMatchButton == krabbButton) {
			if (firstMatchButton == krabbTextButton
					|| secondMatchButton == krabbTextButton) {
				return true;
			}
		} else if (firstMatchButton == apaButton
				|| secondMatchButton == apaButton) {
			if (firstMatchButton == apaTextButton
					|| secondMatchButton == apaTextButton) {
				return true;
			}
		} else if (firstMatchButton == elefantButton
				|| secondMatchButton == elefantButton) {
			if (firstMatchButton == elefantTextButton
					|| secondMatchButton == elefantTextButton) {
				return true;
			}
		} else if (firstMatchButton == farButton
				|| secondMatchButton == farButton) {
			if (firstMatchButton == farTextButton
					|| secondMatchButton == farTextButton) {
				return true;
			}
		} else if (firstMatchButton == grisButton
				|| secondMatchButton == grisButton) {
			if (firstMatchButton == grisTextButton
					|| secondMatchButton == grisTextButton) {
				return true;
			}
		} else if (firstMatchButton == krokodilButton
				|| secondMatchButton == krokodilButton) {
			if (firstMatchButton == krokodilTextButton
					|| secondMatchButton == krokodilTextButton) {
				return true;
			}
		} else if (firstMatchButton == lejonButton
				|| secondMatchButton == lejonButton) {
			if (firstMatchButton == lejonTextButton
					|| secondMatchButton == lejonTextButton) {
				return true;
			}
		} else if (firstMatchButton == ormButton
				|| secondMatchButton == ormButton) {
			if (firstMatchButton == ormTextButton
					|| secondMatchButton == ormTextButton) {
				return true;
			}
		}

		return false;
	}

	public void onShakeDetected(double force) {

	}

	public void allowTurn() {

	}

	public void disallowTurn() {

	}

	// adds all the buttons to the allowedlist
	public void addButtonsToList() {
		buttonList[0] = koButton;
		buttonList[1] = koTextButton;
		buttonList[2] = krabbButton;
		buttonList[3] = krabbTextButton;
		buttonList[4] = apaButton;
		buttonList[5] = apaTextButton;
		buttonList[6] = elefantButton;
		buttonList[7] = elefantTextButton;
		buttonList[8] = farButton;
		buttonList[9] = farTextButton;
		buttonList[10] = grisButton;
		buttonList[11] = grisTextButton;
		buttonList[12] = krokodilButton;
		buttonList[13] = krokodilTextButton;
		buttonList[14] = lejonButton;
		buttonList[15] = lejonTextButton;
		buttonList[16] = ormButton;
		buttonList[17] = ormTextButton;
		for (int i = 0; i < 18; i++) {
			allowedList.add(buttonList[i]);
		}
	}

	public boolean onTouch(View arg0, MotionEvent arg1) {
		if (arg1.getActionMasked() == 0) {
			mShakeNotifier.pressed();
		} else if (arg1.getActionMasked() == 1) {
			if (readyToRemove) {
				remove = true;
				readyToRemove = false;
			}

			mShakeNotifier.removePressed();
			mShakeNotifier.allowTurn();
		}
		return false;
	}

	// called when the user tilts the cell phone to the right, then selects the
	// next brick
	public void moveRight() {

		boolean found = false;
		if (col < 2) {
			col++;

			int currentId = pressedButton.getId();
			int nextId = currentId + 1;

			pressedButton.setImageResource(R.drawable.backgroundmemory);

			for (int i = 0; i < 18; i++) {

				if (buttonList[i].getId() == nextId) {
					if (allowedList.contains(buttonList[i])) {
						pressedButton = buttonList[i];
						found = true;
						v.vibrate(50);
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
						break;
					} else if (col == 1) {

						nextId++;

						for (int k = 0; k < 18; k++) {

							if (buttonList[k].getId() == nextId) {
								if (allowedList.contains(buttonList[k])) {
									col++;
									pressedButton = buttonList[k];
									v.vibrate(50);
									found = true;
									pressedButton
											.setImageResource(R.drawable.backgroundmemory2);
									break;
								} else {

									col--;
									pressedButton
											.setImageResource(R.drawable.backgroundmemory2);
								}
							}

						}

					}
					if (!found) {

						while (!found) {

							nextId++;
							if (nextId > R.id.imageButton18) {
								found = false;
								break;

							}
							for (int k = 0; k < 18; k++) {

								if (buttonList[k].getId() == nextId) {

									if (allowedList.contains(buttonList[k])) {
										v.vibrate(50);
										pressedButton
												.setImageResource(R.drawable.backgroundmemory);
										pressedButton = buttonList[k];
										pressedButton
												.setImageResource(R.drawable.backgroundmemory2);
										found = true;
										fix();
										break;

									}

								}

							}

						}

					}
					if (!found) {
						fix();
						col--;
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
					}
				}
			}

		} else {
			found = false;
			int currentId = pressedButton.getId();
			int nextId = currentId + 1;
			pressedButton.setImageResource(R.drawable.backgroundmemory);
			while (!found) {

				nextId++;
				if (nextId > R.id.imageButton18) {
					found = false;
					break;

				}
				for (int k = 0; k < 18; k++) {

					if (buttonList[k].getId() == nextId) {

						if (allowedList.contains(buttonList[k])) {
							v.vibrate(50);
							pressedButton = buttonList[k];
							pressedButton
									.setImageResource(R.drawable.backgroundmemory2);
							found = true;
							fix();
							break;

						}

					}

				}

			}

		}
		if (!found) {
			col--;
			pressedButton.setImageResource(R.drawable.backgroundmemory2);

		}
	}

	// called when the user tilts the cell phone to the left, then selects the
	// next brick
	public void moveLeft() {

		boolean found = false;
		if (col > 0) {
			col--;

			int currentId = pressedButton.getId();
			int nextId = currentId - 1;

			pressedButton.setImageResource(R.drawable.backgroundmemory);

			for (int i = 0; i < 18; i++) {

				if (buttonList[i].getId() == nextId) {
					if (allowedList.contains(buttonList[i])) {
						pressedButton = buttonList[i];
						v.vibrate(50);
						found = true;
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
						break;
					} else if (col == 1) {

						nextId--;

						for (int k = 0; k < 18; k++) {

							if (buttonList[k].getId() == nextId) {
								if (allowedList.contains(buttonList[k])) {
									col--;
									pressedButton = buttonList[k];
									v.vibrate(50);
									found = true;
									pressedButton
											.setImageResource(R.drawable.backgroundmemory2);
									break;
								} else {

									col++;
									pressedButton
											.setImageResource(R.drawable.backgroundmemory2);
								}

							}

						}

					}
					if (!found) {

						while (!found) {

							nextId--;
							if (nextId < R.id.imageButton1) {
								found = false;
								break;
							}
							for (int k = 0; k < 18; k++) {

								if (buttonList[k].getId() == nextId) {

									if (allowedList.contains(buttonList[k])) {
										v.vibrate(50);
										pressedButton
												.setImageResource(R.drawable.backgroundmemory);
										pressedButton = buttonList[k];
										pressedButton
												.setImageResource(R.drawable.backgroundmemory2);
										found = true;
										fix();
										break;

									}

								}

							}

						}

					}

					if (!found) {

						col++;
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
					}
				}
			}

		} else {
			found = false;
			int currentId = pressedButton.getId();
			int nextId = currentId - 1;
			pressedButton.setImageResource(R.drawable.backgroundmemory);
			while (!found) {

				nextId--;
				if (nextId < R.id.imageButton1) {
					found = false;
					break;

				}
				for (int k = 0; k < 18; k++) {

					if (buttonList[k].getId() == nextId) {

						if (allowedList.contains(buttonList[k])) {
							v.vibrate(50);
							pressedButton = buttonList[k];
							pressedButton
									.setImageResource(R.drawable.backgroundmemory2);
							found = true;
							fix();
							break;

						}

					}

				}

			}

		}
		if (!found) {
			fix();
			col++;
			pressedButton.setImageResource(R.drawable.backgroundmemory2);

		}

	}

	// called when the user tilts the cell phone downwards, then selects the
	// next brick
	public void moveDown() {

		boolean specialCase = true;
		if (row < 5) {
			row++;
			int currentId = pressedButton.getId();
			int nextId = currentId + 4;
			pressedButton.setImageResource(R.drawable.backgroundmemory);
			boolean found = false;
			for (int i = 0; i < 18; i++) {

				if (buttonList[i].getId() == nextId) {
					if (allowedList.contains(buttonList[i])) {
						specialCase = false;
						v.vibrate(50);
						found = true;
						pressedButton = buttonList[i];
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
					}

					else if (!(row == 5)) {
						int checkedRows = row;
						int tempId = nextId;

						while (!found && checkedRows != 5) {
							checkedRows++;

							tempId = tempId + 4;
							for (int k = 0; k < 18; k++) {

								if (buttonList[k].getId() == tempId) {

									if (allowedList.contains(buttonList[k])) {

										pressedButton = buttonList[k];
										pressedButton
												.setImageResource(R.drawable.backgroundmemory2);
										v.vibrate(50);
										found = true;
										row = checkedRows;
										specialCase = false;
										break;
									}

								}
							}
						}

					}
					if (specialCase) {

						nextId = nextId - (col + 1);

						while (!found) {

							nextId++;
							if (nextId > R.id.imageButton18) {
								found = false;
								break;

							}
							for (int k = 0; k < 18; k++) {

								if (buttonList[k].getId() == nextId) {

									if (allowedList.contains(buttonList[k])) {
										v.vibrate(50);
										pressedButton = buttonList[k];
										pressedButton
												.setImageResource(R.drawable.backgroundmemory2);
										found = true;
										fix();
										break;

									}

								}

							}

						}

					}
					if (!found) {

						row--;
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
					}

				}
			}

		}

	}

	// called when the user tilts the cell phone upwards, then selects the
	// next brick
	public void moveUp() {

		boolean specialCase = true;
		if (row > 0) {
			row--;
			int currentId = pressedButton.getId();
			int nextId = currentId - 4;
			pressedButton.setImageResource(R.drawable.backgroundmemory);
			boolean found = false;
			for (int i = 0; i < 18; i++) {

				if (buttonList[i].getId() == nextId) {
					if (allowedList.contains(buttonList[i])) {
						specialCase = false;
						found = true;
						pressedButton = buttonList[i];
						v.vibrate(50);
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
					}

					else if (!(row == 0)) {
						int checkedRows = row;
						int tempId = nextId;

						while (!found && checkedRows != 0) {
							checkedRows--;

							tempId = tempId - 4;
							for (int k = 0; k < 18; k++) {

								if (buttonList[k].getId() == tempId) {

									if (allowedList.contains(buttonList[k])) {

										v.vibrate(50);
										pressedButton = buttonList[k];
										pressedButton
												.setImageResource(R.drawable.backgroundmemory2);
										found = true;
										row = checkedRows;
										specialCase = false;
										break;
									}

								}

							}

						}
					}
					if (specialCase) {

						nextId = nextId + (3 - col);

						while (!found) {

							nextId--;
							if (nextId < R.id.imageButton1) {
								found = false;
								break;
							}
							for (int k = 0; k < 18; k++) {

								if (buttonList[k].getId() == nextId) {

									if (allowedList.contains(buttonList[k])) {
										v.vibrate(50);
										pressedButton = buttonList[k];
										pressedButton
												.setImageResource(R.drawable.backgroundmemory2);
										found = true;
										fix();
										break;

									}

								}

							}

						}

					}
					if (!found) {
						row++;
						pressedButton
								.setImageResource(R.drawable.backgroundmemory2);
					}

				}
			}

		}

	}

	// called when a new brick has been selected in order to set the correct col
	// and row
	private void fix() {

		int tempId = pressedButton.getId();
		if (tempId == R.id.imageButton1 || tempId == R.id.imageButton4
				|| tempId == R.id.imageButton7 || tempId == R.id.imageButton10
				|| tempId == R.id.imageButton13 || tempId == R.id.imageButton16) {
			col = 0;

		} else if (tempId == R.id.imageButton2 || tempId == R.id.imageButton5
				|| tempId == R.id.imageButton8 || tempId == R.id.imageButton11
				|| tempId == R.id.imageButton14 || tempId == R.id.imageButton17) {
			col = 1;

		} else {
			col = 2;

		}

		if (tempId == R.id.imageButton1 || tempId == R.id.imageButton2
				|| tempId == R.id.imageButton3) {
			row = 0;
		} else if (tempId == R.id.imageButton4 || tempId == R.id.imageButton5
				|| tempId == R.id.imageButton6) {
			row = 1;

		} else if (tempId == R.id.imageButton7 || tempId == R.id.imageButton8
				|| tempId == R.id.imageButton9) {
			row = 2;

		} else if (tempId == R.id.imageButton10 || tempId == R.id.imageButton11
				|| tempId == R.id.imageButton12) {
			row = 3;

		} else if (tempId == R.id.imageButton13 || tempId == R.id.imageButton14
				|| tempId == R.id.imageButton15) {
			row = 4;
		} else {
			row = 5;
		}

	}

	// called when restart in Game Activity
	// pause mShakeNotifier so it doesnt listen for old things, does a on
	// create with savedInstanceState from first start
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.icontext:
			mShakeNotifier.onStop();
			onCreate(savedInstanceState);
			return true;

		}
		return false;
	}
}